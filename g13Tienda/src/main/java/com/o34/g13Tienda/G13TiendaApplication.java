package com.o34.g13Tienda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class G13TiendaApplication {

	public static void main(String[] args) {
		SpringApplication.run(G13TiendaApplication.class, args);
	}

}
