package com.o34.g13Tienda.Implement;

import com.o34.g13Tienda.Dao.VentaDao;
import com.o34.g13Tienda.Models.Venta;
import com.o34.g13Tienda.Service.VentaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VentaServiceImpl implements VentaService{
    @Autowired
    private VentaDao ventaDao;
    
    @Override
    @Transactional(readOnly=false)
    public Venta save(Venta venta){
        return ventaDao.save(venta);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        ventaDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Venta>findAll(){
        return (List<Venta>) ventaDao.findAll();
    }
    
    @Override
    @Transactional(readOnly=true)
    public Venta findById(Integer id){
        return ventaDao.findById(id).orElse(null);
    }
}
