package com.o34.g13Tienda.Implement;

import com.o34.g13Tienda.Service.RolService;
import com.o34.g13Tienda.Models.Rol;
import com.o34.g13Tienda.Dao.RolDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RolServiceImpl implements RolService {
    @Autowired
    private RolDao rolDao;
    
    @Override
    @Transactional(readOnly=false)
    public Rol save(Rol rol){
        return rolDao.save(rol);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        rolDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Rol>findAll(){
        return (List<Rol>) rolDao.findAll();
    }
    
    @Override
    @Transactional(readOnly=true)
    public Rol findById(Integer id){
        return rolDao.findById(id).orElse(null);
    }
    
}
