package com.o34.g13Tienda.Implement;

import com.o34.g13Tienda.Service.PersonaService;
import com.o34.g13Tienda.Models.Persona;
import com.o34.g13Tienda.Dao.PersonaDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonaServiceImpl implements PersonaService {
    @Autowired
    private PersonaDao personaDao;
    
    @Override
    @Transactional(readOnly=false)
    public Persona save(Persona persona){
        return personaDao.save(persona);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        personaDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Persona>findAll(){
        return (List<Persona>) personaDao.findAll();
    }
    
    @Override
    @Transactional(readOnly=true)
    public Persona findById(Integer id){
        return personaDao.findById(id).orElse(null);
    }
    
}
