package com.o34.g13Tienda.Implement;

import com.o34.g13Tienda.Dao.DetallecompraDao;
import com.o34.g13Tienda.Models.Detallecompra;

import com.o34.g13Tienda.Service.DetallecompraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DetallecompraServiceImpl implements DetallecompraService{
    @Autowired
    private DetallecompraDao detallecompraDao;
    
    @Override
    @Transactional(readOnly=false)
    public Detallecompra save(Detallecompra detallecompra){
        return detallecompraDao.save(detallecompra);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        detallecompraDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Detallecompra>findAll(){
        return (List<Detallecompra>) detallecompraDao.findAll();
    }
    
    @Override
    @Transactional(readOnly=true)
    public Detallecompra findById(Integer id){
        return detallecompraDao.findById(id).orElse(null);
    }
}
