package com.o34.g13Tienda.Implement;

import com.o34.g13Tienda.Dao.CompraDao;
import com.o34.g13Tienda.Models.Compra;
import com.o34.g13Tienda.Service.CompraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CompraServiceImpl implements CompraService{
    @Autowired
    private CompraDao compraDao;
    
    @Override
    @Transactional(readOnly=false)
    public Compra save(Compra compra){
        return compraDao.save(compra);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        compraDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Compra>findAll(){
        return (List<Compra>) compraDao.findAll();
    }
    
    @Override
    @Transactional(readOnly=true)
    public Compra findById(Integer id){
        return compraDao.findById(id).orElse(null);
    }
}
