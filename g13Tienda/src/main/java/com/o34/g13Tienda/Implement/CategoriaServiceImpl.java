package com.o34.g13Tienda.Implement;

import com.o34.g13Tienda.Service.CategoriaService;
import com.o34.g13Tienda.Models.Categoria;
import com.o34.g13Tienda.Dao.CategoriaDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoriaServiceImpl implements CategoriaService {
    @Autowired
    private CategoriaDao categoriaDao;
    
    @Override
    @Transactional(readOnly=false)
    public Categoria save(Categoria categoria){
        return categoriaDao.save(categoria);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        categoriaDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Categoria>findAll(){
        return (List<Categoria>) categoriaDao.findAll();
    }
    
    @Override
    @Transactional(readOnly=true)
    public Categoria findById(Integer id){
        return categoriaDao.findById(id).orElse(null);
    }
    
}
