package com.o34.g13Tienda.Implement;

import com.o34.g13Tienda.Dao.DetalleventaDao;
import com.o34.g13Tienda.Models.Detalleventa;

import com.o34.g13Tienda.Service.DetalleventaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DetalleventaServiceImpl implements DetalleventaService{
    @Autowired
    private DetalleventaDao detalleventaDao;
    
    @Override
    @Transactional(readOnly=false)
    public Detalleventa save(Detalleventa detalleventa){
        return detalleventaDao.save(detalleventa);
    }
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        detalleventaDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Detalleventa>findAll(){
        return (List<Detalleventa>) detalleventaDao.findAll();
    }
    
    @Override
    @Transactional(readOnly=true)
    public Detalleventa findById(Integer id){
        return detalleventaDao.findById(id).orElse(null);
    }
}
