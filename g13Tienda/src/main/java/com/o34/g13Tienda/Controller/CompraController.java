package com.o34.g13Tienda.Controller;

import com.o34.g13Tienda.Models.Compra;
import com.o34.g13Tienda.Service.CompraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/compras")
public class CompraController {
       @Autowired
    private CompraService compraservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Compra> agregar(@RequestBody Compra compra){
        Compra obj = compraservice.save(compra);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Compra> eliminar(@PathVariable Integer id){
        Compra obj = compraservice.findById(id);
        if(obj!=null)
            compraservice.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Compra> editar(@RequestBody Compra compra){
        Compra obj = compraservice.findById(compra.getIdCompra());
        if(obj!=null){
            obj.setPersona(compra.getPersona());
            obj.setNumeroFactura(compra.getNumeroFactura());
            obj.setFechaCompra(compra.getFechaCompra());
            obj.setValorImpuesto(compra.getValorImpuesto()); 
            obj.setValorCompra(compra.getValorCompra());
            obj.setUsuario(compra.getUsuario());
            compraservice.save(obj);
        }
        else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Compra> consultarTodo(){
        return compraservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Compra consultarPorId(@PathVariable Integer id){
        return compraservice.findById(id);
    }
}
