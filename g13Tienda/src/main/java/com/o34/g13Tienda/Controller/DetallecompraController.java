package com.o34.g13Tienda.Controller;

import com.o34.g13Tienda.Models.Detallecompra;
import com.o34.g13Tienda.Service.DetallecompraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/detallecompras")
public class DetallecompraController {
       @Autowired
    private DetallecompraService detallecompraservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Detallecompra> agregar(@RequestBody Detallecompra detallecompra){
        Detallecompra obj = detallecompraservice.save(detallecompra);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Detallecompra> eliminar(@PathVariable Integer id){
        Detallecompra obj = detallecompraservice.findById(id);
        if(obj!=null)
            detallecompraservice.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Detallecompra> editar(@RequestBody Detallecompra detallecompra){
        Detallecompra obj = detallecompraservice.findById(detallecompra.getIdDetalleCompra());
        if(obj!=null){
            obj.setCompra(detallecompra.getCompra());
            obj.setProducto(detallecompra.getProducto());
            obj.setCantidadProducto(detallecompra.getCantidadProducto());
            obj.setPrecioProducto(detallecompra.getPrecioProducto()); 
            obj.setPrecioVenta(detallecompra.getPrecioVenta());
            obj.setPorcentajeImpuesto(detallecompra.getPorcentajeImpuesto()); 
            detallecompraservice.save(obj);
        }
        else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Detallecompra> consultarTodo(){
        return detallecompraservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Detallecompra consultarPorId(@PathVariable Integer id){
        return detallecompraservice.findById(id);
    }
}
