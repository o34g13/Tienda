package com.o34.g13Tienda.Controller;

import com.o34.g13Tienda.Models.Producto;
import com.o34.g13Tienda.Service.ProductoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/productos")
public class ProductoController {
    @Autowired
    private ProductoService productoservice;
    
    @GetMapping("/list")
    public List<Producto> consultarTodo(){
        return productoservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Producto consultarPorId(@PathVariable Integer id){
        return productoservice.findById(id);
    }

    @PostMapping(value="/")
    public ResponseEntity<Producto> agregar(@RequestBody Producto producto){
        Producto obj = productoservice.save(producto);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Producto> eliminar(@PathVariable Integer id){
        Producto obj = productoservice.findById(id);
        if(obj!=null)
            productoservice.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Producto> editar(@RequestBody Producto producto){
        Producto obj = productoservice.findById(producto.getIdProducto());
        if(obj!=null){
           obj.setCategoria(producto.getCategoria());
            obj.setDescripcionProducto(producto.getDescripcionProducto());
            obj.setCantidadProducto(producto.getCantidadProducto());
            obj.setValorUnitarioProducto(producto.getValorUnitarioProducto());
            obj.setPorcentajeImpuesto(producto.getPorcentajeImpuesto()); 
            obj.setExistenciaProducto(producto.getExistenciaProducto());
            obj.setEstadoProducto(producto.getEstadoProducto());
            productoservice.save(obj);
        }
        else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    

}
