package com.o34.g13Tienda.Controller;

import com.o34.g13Tienda.Models.Persona;
import com.o34.g13Tienda.Service.PersonaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/personas")
public class PersonaController {
    @Autowired
    private PersonaService personaservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Persona> agregar(@RequestBody Persona persona){
        Persona obj = personaservice.save(persona);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Persona> eliminar(@PathVariable Integer id){
        Persona obj = personaservice.findById(id);
        if(obj!=null)
            personaservice.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Persona> editar(@RequestBody Persona persona){
        Persona obj = personaservice.findById(persona.getIdPersona());
        if(obj!=null){
            obj.setTipoPersona(persona.getTipoPersona());
            obj.setApellidoPersona(persona.getApellidoPersona());
            obj.setNombrePersona(persona.getNombrePersona());
            obj.setTipoDocumentoPersona(persona.getTipoDocumentoPersona()); 
            obj.setNumeroDocumentoPersona(persona.getNumeroDocumentoPersona());
            obj.setDireccionPersona(persona.getDireccionPersona());
            obj.setTelefonoPersona(persona.getTelefonoPersona());
            obj.setEmailPersona(persona.getEmailPersona());
            personaservice.save(obj);
        }
        else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Persona> consultarTodo(){
        return personaservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Persona consultarPorId(@PathVariable Integer id){
        return personaservice.findById(id);
    }
}
