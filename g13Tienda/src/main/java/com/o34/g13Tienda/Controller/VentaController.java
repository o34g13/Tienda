package com.o34.g13Tienda.Controller;

import com.o34.g13Tienda.Models.Venta;
import com.o34.g13Tienda.Service.VentaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/ventas")
public class VentaController {
       @Autowired
    private VentaService ventaservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Venta> agregar(@RequestBody Venta venta){
        Venta obj = ventaservice.save(venta);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Venta> eliminar(@PathVariable Integer id){
        Venta obj = ventaservice.findById(id);
        if(obj!=null)
            ventaservice.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Venta> editar(@RequestBody Venta venta){
        Venta obj = ventaservice.findById(venta.getIdVenta());
        if(obj!=null){
            obj.setPersona(venta.getPersona());
            obj.setNumeroFactura(venta.getNumeroFactura());
            obj.setFechaVenta(venta.getFechaVenta());
            obj.setValorImpuesto(venta.getValorImpuesto()); 
            obj.setValorVenta(venta.getValorVenta());
            obj.setUsuario(venta.getUsuario());
            ventaservice.save(obj);
        }
        else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Venta> consultarTodo(){
        return ventaservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Venta consultarPorId(@PathVariable Integer id){
        return ventaservice.findById(id);
    }
}
