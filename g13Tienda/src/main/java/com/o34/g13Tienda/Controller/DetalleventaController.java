package com.o34.g13Tienda.Controller;

import com.o34.g13Tienda.Models.Detalleventa;
import com.o34.g13Tienda.Service.DetalleventaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/detalleventas")
public class DetalleventaController {
       @Autowired
    private DetalleventaService detalleventaservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Detalleventa> agregar(@RequestBody Detalleventa detalleventa){
        Detalleventa obj = detalleventaservice.save(detalleventa);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Detalleventa> eliminar(@PathVariable Integer id){
        Detalleventa obj = detalleventaservice.findById(id);
        if(obj!=null)
            detalleventaservice.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Detalleventa> editar(@RequestBody Detalleventa detalleventa){
        Detalleventa obj = detalleventaservice.findById(detalleventa.getIdDetalleVenta());
        if(obj!=null){
            obj.setVenta(detalleventa.getVenta());
            obj.setProducto(detalleventa.getProducto());
            obj.setCantidadProducto(detalleventa.getCantidadProducto());
            obj.setPrecioProducto(detalleventa.getPrecioProducto()); 
            obj.setPrecioVenta(detalleventa.getPrecioVenta());
            obj.setPorcentajeImpuesto(detalleventa.getPorcentajeImpuesto()); 
            detalleventaservice.save(obj);
        }
        else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Detalleventa> consultarTodo(){
        return detalleventaservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Detalleventa consultarPorId(@PathVariable Integer id){
        return detalleventaservice.findById(id);
    }
}
