package com.o34.g13Tienda.Controller;

import com.o34.g13Tienda.Models.Categoria;
import com.o34.g13Tienda.Service.CategoriaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/categorias")
public class CategoriaController {
    @Autowired
    private CategoriaService categoriaservice;
    
   @GetMapping("/list")
    public List<Categoria> consultarTodo(){
        return categoriaservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Categoria consultarPorId(@PathVariable Integer id){
        return categoriaservice.findById(id);
    }

    @PostMapping(value="/")
    public ResponseEntity<Categoria> agregar(@RequestBody Categoria categoria){
        Categoria obj = categoriaservice.save(categoria);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Categoria> eliminar(@PathVariable Integer id){
        Categoria obj = categoriaservice.findById(id);
        if(obj!=null)
            categoriaservice.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Categoria> editar(@RequestBody Categoria categoria){
        Categoria obj = categoriaservice.findById(categoria.getIdCategoria());
        if(obj!=null){
            obj.setNombreCategoria(categoria.getNombreCategoria());
            obj.setDescripcionCategoria(categoria.getDescripcionCategoria());
            obj.setEstadoCategoria(categoria.getEstadoCategoria());
            categoriaservice.save(obj);
        }
        else{
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
 
}
