package com.o34.g13Tienda.Dao;

import com.o34.g13Tienda.Models.Detalleventa;
import org.springframework.data.repository.CrudRepository;

public interface DetalleventaDao extends 
        CrudRepository<Detalleventa, Integer> {
    
}
