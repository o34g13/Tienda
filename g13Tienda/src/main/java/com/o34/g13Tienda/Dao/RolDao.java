package com.o34.g13Tienda.Dao;

import com.o34.g13Tienda.Models.Rol;
import org.springframework.data.repository.CrudRepository;

public interface RolDao extends 
        CrudRepository<Rol, Integer> {
    
}
