package com.o34.g13Tienda.Dao;

import com.o34.g13Tienda.Models.Venta;
import org.springframework.data.repository.CrudRepository;

public interface VentaDao extends 
        CrudRepository<Venta, Integer> {
    
}
