package com.o34.g13Tienda.Dao;

import com.o34.g13Tienda.Models.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaDao extends
        CrudRepository<Persona, Integer> {
    
}
