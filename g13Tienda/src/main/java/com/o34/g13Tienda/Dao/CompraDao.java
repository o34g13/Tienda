package com.o34.g13Tienda.Dao;

import com.o34.g13Tienda.Models.Compra;
import org.springframework.data.repository.CrudRepository;

public interface CompraDao extends 
        CrudRepository<Compra, Integer> {
    
}
