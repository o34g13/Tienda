package com.o34.g13Tienda.Dao;

import com.o34.g13Tienda.Models.Categoria;
import org.springframework.data.repository.CrudRepository;

public interface CategoriaDao extends 
        CrudRepository<Categoria, Integer> {
    
}
