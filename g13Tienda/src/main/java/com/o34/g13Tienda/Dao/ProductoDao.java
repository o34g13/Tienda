package com.o34.g13Tienda.Dao;

import com.o34.g13Tienda.Models.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductoDao extends CrudRepository<Producto, Integer> {
    
}
