package com.o34.g13Tienda.Dao;

import com.o34.g13Tienda.Models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioDao extends 
        CrudRepository<Usuario, Integer> {
    
}
