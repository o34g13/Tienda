package com.o34.g13Tienda.Dao;

import com.o34.g13Tienda.Models.Detallecompra;
import org.springframework.data.repository.CrudRepository;

public interface DetallecompraDao extends 
        CrudRepository<Detallecompra, Integer> {
    
}
