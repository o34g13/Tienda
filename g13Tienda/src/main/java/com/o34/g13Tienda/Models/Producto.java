
package com.o34.g13Tienda.Models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
@Table(name="producto")
public class Producto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idProducto")
    private Integer idProducto;
    
    @ManyToOne
    @JoinColumn(name="idCategoria")
    private Categoria categoria;

    @Column(name="descripcionProducto")
    private String descripcionProducto;
    
    @Column(name="cantidadProducto")
    private double cantidadProducto;

    @Column(name="valorUnitarioProducto")
    private double valorUnitarioProducto;

    @Column(name="porcentajeImpuesto")
    private double porcentajeImpuesto;

    @Column(name="existenciaProducto")
    private double existenciaProducto;

    @Column(name="estadoProducto")
    private Integer estadoProducto;

    public Integer getIdProducto() {
        return idProducto;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public String getDescripcionProducto() {
        return descripcionProducto;
    }

    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }

    public double getCantidadProducto() {
        return cantidadProducto;
    }

    public void setCantidadProducto(double cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }

    public double getValorUnitarioProducto() {
        return valorUnitarioProducto;
    }

    public void setValorUnitarioProducto(double valorUnitarioProducto) {
        this.valorUnitarioProducto = valorUnitarioProducto;
    }

    public double getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(double porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public double getExistenciaProducto() {
        return existenciaProducto;
    }

    public void setExistenciaProducto(double existenciaProducto) {
        this.existenciaProducto = existenciaProducto;
    }

    public Integer getEstadoProducto() {
        return estadoProducto;
    }

    public void setEstadoProducto(Integer estadoProducto) {
        this.estadoProducto = estadoProducto;
    }

}
