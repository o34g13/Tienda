package com.o34.g13Tienda.Service;

import com.o34.g13Tienda.Models.Venta;
import java.util.List;

public interface VentaService {
    public Venta save(Venta compra);
    public void delete(Integer id);
    public Venta findById(Integer id);
    public List<Venta> findAll();
}
