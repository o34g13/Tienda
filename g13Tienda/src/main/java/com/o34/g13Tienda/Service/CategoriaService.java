package com.o34.g13Tienda.Service;

import com.o34.g13Tienda.Models.Categoria;
import java.util.List;

public interface CategoriaService {
    public Categoria save(Categoria categoria);
    public void delete(Integer id);
    public Categoria findById(Integer id);
    public List<Categoria> findAll();
}
