package com.o34.g13Tienda.Service;

import com.o34.g13Tienda.Models.Producto;
import java.util.List;

public interface ProductoService {
    public Producto save(Producto producto);
    public void delete(Integer id);
    public Producto findById(Integer id);
    public List<Producto> findAll();
}
