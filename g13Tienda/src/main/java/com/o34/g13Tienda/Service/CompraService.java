package com.o34.g13Tienda.Service;

import com.o34.g13Tienda.Models.Compra;
import java.util.List;

public interface CompraService {
    public Compra save(Compra compra);
    public void delete(Integer id);
    public Compra findById(Integer id);
    public List<Compra> findAll();
}
