package com.o34.g13Tienda.Service;

import com.o34.g13Tienda.Models.Detallecompra;
import java.util.List;

public interface DetallecompraService {
    public Detallecompra save(Detallecompra detallecompra);
    public void delete(Integer id);
    public Detallecompra findById(Integer id);
    public List<Detallecompra> findAll();
}
