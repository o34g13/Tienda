package com.o34.g13Tienda.Service;

import com.o34.g13Tienda.Models.Rol;
import java.util.List;

public interface RolService {
    public Rol save(Rol rol);
    public void delete(Integer id);
    public Rol findById(Integer id);
    public List<Rol> findAll();
}
