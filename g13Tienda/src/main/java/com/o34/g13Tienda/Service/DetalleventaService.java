package com.o34.g13Tienda.Service;

import com.o34.g13Tienda.Models.Detalleventa;
import java.util.List;

public interface DetalleventaService {
    public Detalleventa save(Detalleventa detalleventa);
    public void delete(Integer id);
    public Detalleventa findById(Integer id);
    public List<Detalleventa> findAll();
}
