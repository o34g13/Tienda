let gidVenta;
let gidCliente;
let gidnumeroFactura;
let gfechaVenta;
let gidUsuario;
function loadData(){
    let request = sendRequest('ventas/list', 'GET', '');
    let table = document.getElementById('ventas-table');
    let valorIdVenta = document.getElementById('ventaid');
    document.getElementById("btnGrabaFactura").style.visibility="hidden";
    //let id= document.getElementById('venta-id')
    //console.log(valorIdVenta);
    //console.log("valorIdVenta",valorIdVenta.value);
    table.innerHTML = "";
    request.onload = function(){
        let data = request.response;

        data.forEach((element, index) => {
            if (element.valorVenta == 0){    
                document.getElementById("btnNuevo").style.visibility="hidden";
                valorIdVenta.value = element.idVenta
                gidVenta = element.idVenta
                gidCliente = element.persona.idPersona
                gidnumeroFactura = element.numeroFactura                
                gfechaVenta = Date.now()
                gidUsuario = element.usuario.idUsuario
                table.innerHTML += `
                    <tr >
                        <td>${element.idVenta}</td>
                        <td>${element.persona.nombrePersona}</td>
                        <td>${element.numeroFactura}</td>
                        <td>${element.fechaVenta}</td>
                        <td>${element.valorImpuesto}</td>
                        <td>${element.valorDescuento}</td>
                        <td>${element.valorVenta}</td>
                        <td>${element.usuario.nombreUsuario}</td>
                        <td>
                            <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#registra_producto">Adiciona Producto</button>
                            <button type="button" class="btn  btn-link" 
                            onclick='window.location = "formventas.html?id=${element.idVenta}"+"&idt=${element.persona.idPersona}"'>
                            Editar                            
                            </button>
                            <button type="button" class="btn btn-danger" onclick='deleteVenta(${element.idVenta})'>
                            Borrar                        
                            </button>
                            
                        </td>
                    </tr>
                    `
                    leerDetalles(valorIdVenta.value);

            }
            else {

            }
        });

 
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
            `;
    }


}

function saveVenta(){
    let cliente= document.getElementById('cliente').value; 
    let factura = document.getElementById('venta-factura').value;
    let fechac = document.getElementById('venta-fecha').value;
    let vrimpuesto= document.getElementById('venta-impuesto').value;
    let vrdescuento = 0; //document.getElementById('venta-vrdescuento').value;
    let vrventa = document.getElementById('venta-valor').value;
    let usuario = "2" ; //document.getElementById('venta-usuario').value;
    let id = document.getElementById('venta-id').value;
    let data = {'idVenta': id,'persona' : {'idPersona': cliente }, 'numeroFactura': factura, 'fechaVenta': fechac,'valorImpuesto': vrimpuesto, 'valorDescuento': vrdescuento, 'valorVenta': vrventa, 'usuario': {'idUsuario': usuario}  }
    console.log(data);
    let request = sendRequest('ventas/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'ventas.html';
    } 
    request.onerror = function(){
        alert('Error al guardar los cambios')
    }
}

function grabaFactura(){

    let cliente= gidCliente; ///document.getElementById('cliente').value; //producto-categoria
    let factura = gidnumeroFactura;//document.getElementById('venta-factura').value;
    let fechac = gfechaVenta;   ;//;document.getElementById('venta-fecha').value;
    let vrimpuesto= 0;//document.getElementById('venta-impuesto').value;
    let vrdescuento = 0;
    let vrventa = sumaTotal;//document.getElementById('venta-valor').value;
    let usuario = 2 ; //document.getElementById('venta-usuario').value;
    let id = gidVenta; //document.getElementById('venta-id').value;
    let data = {'idVenta': id,'persona' : {'idPersona': cliente }, 'numeroFactura': factura, 'fechaVenta': fechac,'valorImpuesto': vrimpuesto, 'valorDescuento': vrdescuento,'valorVenta': vrventa, 'usuario': {'idUsuario': usuario}  }
    console.log("graba",data);
    let request = sendRequest('ventas/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'ventas.html';
    } 
    request.onerror = function(){
        alert('Error al guardar los cambios')
    }
}


function deleteVenta(idVenta){
    let request = sendRequest('ventas/'+idVenta,'DELETE','')
    request.onload = function(){
        window.location = 'ventas.html';
    }
}


function loadVenta(idVenta,idProv){
    let request = sendRequest('ventas/list/'+idVenta,'GET','')
    let cliente= document.getElementById('cliente');    //producto-categoria
    let factura = document.getElementById('venta-factura');
    let fechac = document.getElementById('venta-fecha');
    let vrimpuesto = document.getElementById('venta-impuesto');
    let vrdescuento= document.getElementById('venta-vrdescuenton');
    let vrventa = document.getElementById('venta-valor');
    let usuario = document.getElementById('venta-usuario');
    let id= document.getElementById('venta-id')
    
   request.onload = function(){
        let data = request.response
        console.log(data)
        id.value = data.idVenta
        cliente.value = data.persona
        factura.value = data.numeroFactura       
        fechac.value = data.fechaVenta
        vrimpuesto.value = data.valorImpuesto
        vrdescuento.value = data.valorDescuento
        vrventa.value = data.valorVenta
        usuario.value = data.usuario.idUsuario    
        loadClientes(idProv)
    }
    request.onerror = function(){
        alert("Error al recuperar los datos");
    }
    
    
}

function saveDetalleVenta(){
    //let productoid = document.getElementById('detalleventa-idProducto').value;
    //alert("detalle")
    let productoid = document.getElementById('tipo').value;
    let cantidadproducto = document.getElementById('detalleventa-cantidadProducto').value;
    let precioproducto= document.getElementById('detalleventa-precioProducto').value;
    let precioventa = document.getElementById('detalleventa-PrecioVenta').value;
    let impuestoventa = document.getElementById('detalleventa-porcentajeImpuesto').value;
    let ventaid = document.getElementById('detalleventa-id').value;
    let id = ""; //document.getElementById('detalleventa-id').value;
    let data = {'idDetalleVenta': id, 'venta' : {'idVenta': ventaid }, 'producto' : {'idProducto': productoid}, 'cantidadProducto': cantidadproducto, 
                'precioProducto': precioproducto,'precioVenta': precioventa, 'porcentajeImpuesto': impuestoventa  }
    console.log(data); 
    let request = sendRequest('detalleventas/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        //document.getElementById("registra_producto").Modal.hide();
        //var modal = document.getElementById('registra_producto');
       //modal.hide;
        //leerDetalles(ventaid);

        window.location = 'ventas.html';
    } 
    request.onerror = function(){
        alert('Error al guardar los cambios')
    }
}

let sumaTotal = 0;
function leerDetalles(idVenta){
    
    let request = sendRequest('detalleventas/list/' , 'GET', '');
    //table.innerHTML = "";
    document.getElementById("detalleventa-id").value = idVenta;
    let valorIdVenta = document.getElementById('detalleventa-id');
    console.log("qqq",valorIdVenta);
    let table = document.getElementById('detalleventas-table')
    request.onload = function(){
        let data = request.response;
        if(data == null) {
            console.log("No data detalleventas")
        }
        else 
        {

            data.forEach((element, index) => {
                if (element.venta.idVenta == idVenta){
                    document.getElementById("btnGrabaFactura").style.visibility="visible";
                    var valorimp = (element.cantidadProducto * element.precioProducto) * element.porcentajeImpuesto/100
                    var valorxprod = (element.cantidadProducto * element.precioProducto) + valorimp;
                    sumaTotal = sumaTotal + valorxprod;
                    table.innerHTML += ` 
                        <tr >
                            <td>${element.idDetalleVenta}</td>
                            <td>${element.venta.idVenta}</td>
                            <td>${element.producto.descripcionProducto}</td>
                            <td>${element.cantidadProducto}</td>
                            <td>${element.precioProducto}</td>
                            <td>${element.precioVenta}</td>
                            <td>${element.porcentajeImpuesto}</td>
                            <td>${valorimp}</td>
                            <td>${valorxprod}</td>
                            <td>
                                <button type="button" class="btn  btn-link" onclick='deleteDetalleVenta(${element.idDetalleVenta})'>Borrar</button>
                            </td>
                        </tr>
                        `
                    }
              });
       }
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
            `;
    }
}

function deleteDetalleVenta(idDetalleVenta){
    let request = sendRequest('detalleventas/'+idDetalleVenta,'DELETE','')
    request.onload = function(){
        window.location = 'ventas.html';
    }
}


