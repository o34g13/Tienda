function loadData(){
    let request = sendRequest('categorias/list', 'GET', '');
    let table = document.getElementById('categorias-table');
    table.innerHTML = "";
    request.onload = function(){
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr >
                    <th>${element.idCategoria}</th>
                    <td>${element.nombreCategoria}</td>
                    <td>${element.descripcionCategoria}</td>
                    <td>${element.estadoCategoria}</td>
                    <td>
                        <button type="button" class="btn  btn-link" 
                        onclick='window.location = "formcategorias.html?id=${element.idCategoria}"'>
                        Editar                            
                        </button>
                        <button type="button" class="btn btn-danger" 
                        onclick='deleteCategoria(${element.idCategoria})'>
                        Borrar                        
                        </button>
                    </td>
                </tr>
                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
            `;
    }
}

function saveCategoria(){
    let nombre = document.getElementById('categoria-nombre').value;
    let descripcion = document.getElementById('categoria-descripcion').value;
    let estado= document.getElementById('estado-categoria').value;
    let id = document.getElementById('categoria-id').value;
    let data = {'idCategoria': id, 'nombreCategoria': nombre, 'descripcionCategoria': descripcion, 'estadoCategoria': estado  }
    let request = sendRequest('categorias/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'categorias.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios')
    }
}

function deleteCategoria(idCategoria){
    let request = sendRequest('categorias/'+idCategoria,'DELETE','')
    request.onload = function(){
        window.location = 'categorias.html';
    }
}

function loadCategoria(idCategoria){
    let request = sendRequest('categorias/list/'+idCategoria,'GET','')
    let nombre = document.getElementById('categoria-nombre');
    let descripcion = document.getElementById('categoria-descripcion');
    let estado= document.getElementById('estado-categoria');
    let id = document.getElementById('categoria-id');
    request.onload = function(){
        let data = request.response
        id.value = data.idCategoria
        nombre.value = data.nombreCategoria
        descripcion.value = data.descripcionCategoria
        estado.value = data.estadoCategoria
    }
    request.onerror = function(){
        alert("Error al recuperar los datos");
    }
}


