function loadData(){
    let request = sendRequest('personas/list', 'GET', '');
    let table = document.getElementById('personas-table');
    table.innerHTML = "";
    request.onload = function(){
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr >
                    <td>${element.idPersona}</td>
                    <td>${element.tipoPersona}</td>
                    <td>${element.apellidoPersona}</td>
                    <td>${element.nombrePersona}</td>
                    <td>${element.tipoDocumentoPersona}</td>
                    <td>${element.numeroDocumentoPersona}</td>
                    <td>${element.direccionPersona}</td>
                    <td>${element.telefonoPersona}</td>
                    <td>${element.emailPersona}</td>
                    <td>
                        <button type="button" class="btn  btn-link" 
                        onclick='window.location = "formpersonas.html?id=${element.idPersona}"'>
                        Editar                            
                        </button>
                        <button type="button" class="btn btn-danger" 
                        onclick='deletePersona(${element.idPersona})'>
                        Borrar                        
                        </button>
                    </td>
                </tr>
                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
            `;
    }
}

function savePersona(){
    let personatipo= document.getElementById('persona-tipo').value;
    let apellidos = document.getElementById('persona-apellidos').value;
    let nombres = document.getElementById('persona-nombres').value;
    let tipodocumento= document.getElementById('persona-tipodocumento').value;
    let documento = document.getElementById('persona-documento').value;
    let direccion = document.getElementById('persona-direccion').value;
    let telefono= document.getElementById('persona-telefono').value;
    let email= document.getElementById('persona-email').value;
    
    let id = document.getElementById('persona-id').value;
    let data = {'idPersona': id, 'tipoPersona': personatipo , 'apellidoPersona' : apellidos, 'nombrePersona': nombres, 'tipoDocumentoPersona': tipodocumento,'numeroDocumentoPersona': documento, 'direccionPersona': direccion,'telefonoPersona':  telefono, 'emailPersona': email }
    console.log(data);
    let request = sendRequest('personas/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'personas.html';
    } 
    request.onerror = function(){
        alert('Error al guardar los cambios')
    }
}

function deletePersona(idPersona){
    let request = sendRequest('personas/'+idPersona,'DELETE','')
    request.onload = function(){
        window.location = 'personas.html';
    }
}

function loadPersona(idPersona){
     console.log(idPersona);
    let request = sendRequest('personas/list/'+idPersona,'GET','')
    let personatipo= document.getElementById('persona-tipo');
    let apellidos = document.getElementById('persona-apellidos');
    let nombres = document.getElementById('persona-nombres');
    let tipodocumento= document.getElementById('persona-tipodocumento');
    let documento = document.getElementById('persona-documento');
    let direccion = document.getElementById('persona-direccion');
    let telefono= document.getElementById('persona-telefono');
    let email= document.getElementById('persona-email');
    let id= document.getElementById('persona-id')
    
   request.onload = function(){
        let data = request.response
        console.log(data)
        id.value = data.idPersona
        personatipo.value = data.tipoPersona
        apellidos.value = data.apellidoPersona       
        nombres.value = data.nombrePersona
        tipodocumento.value = data.tipoDocumentoPersona
        documento.value = data.numeroDocumentoPersona
        direccion.value = data.direccionPersona       
        telefono.value = data.telefonoPersona
        email.value = data.emailPersona
    }
    request.onerror = function(){
        alert("Error al recuperar los datos");
    }

    
}


