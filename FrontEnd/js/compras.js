let gidCompra;
let gidProveedor;
let gidnumeroFactura;
let gfechaCompra;
let gidUsuario;
function loadData(){
    let request = sendRequest('compras/list', 'GET', '');
    let table = document.getElementById('compras-table');
    let valorIdCompra = document.getElementById('compraid');
    document.getElementById("btnGrabaFactura").style.visibility="hidden";
    //let id= document.getElementById('compra-id')
    //console.log(valorIdCompra);
    //console.log("valorIdCompra",valorIdCompra.value);
    table.innerHTML = "";
    request.onload = function(){
        let data = request.response;

        data.forEach((element, index) => {
            if (element.valorCompra == 0){    
                document.getElementById("btnNuevo").style.visibility="hidden";
                valorIdCompra.value = element.idCompra;
                gidCompra = element.idCompra;
                gidProveedor = element.persona.idPersona;
                gidnumeroFactura = element.numeroFactura ;               
                gfechaCompra = Date.now();
                gidUsuario = element.usuario.idUsuario;
                table.innerHTML += `
                    <tr >
                        <td>${element.idCompra}</td>
                        <td>${element.persona.nombrePersona}</td>
                        <td>${element.numeroFactura}</td>
                        <td>${element.fechaCompra}</td>
                        <td>${element.valorImpuesto}</td>
                        <td>${element.valorCompra}</td>
                        <td>${element.usuario.nombreUsuario}</td>
                        <td>
                            <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#registra_producto">Adiciona Producto</button>
                            <button type="button" class="btn  btn-link" 
                            onclick='window.location = "formcompras.html?id=${element.idCompra}"+"&idt=${element.persona.idPersona}"'>
                            Editar                            
                            </button>
                            <button type="button" class="btn btn-danger" onclick='deleteCompra(${element.idCompra})'>
                            Borrar                        
                            </button>
                            
                        </td>
                    </tr>
                    `
                    leerDetalles(valorIdCompra.value);

            }
            else {

            }
        });

 
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
            `;
    }


}

function saveCompra(){
    let proveedor= document.getElementById('proveedor').value; //producto-categoria
    let factura = document.getElementById('compra-factura').value;
    let fechac = document.getElementById('compra-fecha').value;
    let vrimpuesto= document.getElementById('compra-impuesto').value;
    let vrcompra = document.getElementById('compra-valor').value;
    let usuario = "2" ; //document.getElementById('compra-usuario').value;
    let id = document.getElementById('compra-id').value;
    let data = {'idCompra': id,'persona' : {'idPersona': proveedor }, 'numeroFactura': factura, 'fechaCompra': fechac,'valorImpuesto': vrimpuesto, 'valorCompra': vrcompra, 'usuario': {'idUsuario': usuario}  }
    console.log(data);
    let request = sendRequest('compras/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'compras.html';
    } 
    request.onerror = function(){
        alert('Error al guardar los cambios')
    }
}

function grabaFactura(){

    let proveedor= gidProveedor; ///document.getElementById('proveedor').value; //producto-categoria
    let factura = gidnumeroFactura;//document.getElementById('compra-factura').value;
    let fechac = gfechaCompra;   ;//;document.getElementById('compra-fecha').value;
    let vrimpuesto= 0;//document.getElementById('compra-impuesto').value;
    let vrcompra = sumaTotal;//document.getElementById('compra-valor').value;
    let usuario = 2 ; //document.getElementById('compra-usuario').value;
    let id = gidCompra; //document.getElementById('compra-id').value;
    let data = {'idCompra': id,'persona' : {'idPersona': proveedor }, 'numeroFactura': factura, 'fechaCompra': fechac,'valorImpuesto': vrimpuesto, 'valorCompra': vrcompra, 'usuario': {'idUsuario': usuario}  }
    console.log("graba",data);
    let request = sendRequest('compras/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'compras.html';
    } 
    request.onerror = function(){
        alert('Error al guardar los cambios')
    }
}


function deleteCompra(idCompra){
    let request = sendRequest('compras/'+idCompra,'DELETE','')
    request.onload = function(){
        window.location = 'compras.html';
    }
}


function loadCompra(idCompra,idProv){
    let request = sendRequest('compras/list/'+idCompra,'GET','')
    let proveedor= document.getElementById('proveedor');    //producto-categoria
    let factura = document.getElementById('compra-factura');
    let fechac = document.getElementById('compra-fecha');
    let vrimpuesto = document.getElementById('compra-impuesto');
    let vrcompra = document.getElementById('compra-valor');
    let usuario = document.getElementById('compra-usuario');
    let id= document.getElementById('compra-id')
    
   request.onload = function(){
        let data = request.response
        console.log(data)
        id.value = data.idCompra
        proveedor.value = data.persona
        factura.value = data.numeroFactura       
        fechac.value = data.fechaCompra
        vrimpuesto.value = data.valorImpuesto
        vrcompra.value = data.valorCompra
        usuario.value = data.usuario.idUsuario    
        loadProveedores(idProv)
    }
    request.onerror = function(){
        alert("Error al recuperar los datos");
    }
    
    
}

function saveDetalleCompra(){
    //let productoid = document.getElementById('detallecompra-idProducto').value;
    //alert("detalle")
    let productoid = document.getElementById('tipo').value;
    let cantidadproducto = document.getElementById('detallecompra-cantidadProducto').value;
    let precioproducto= document.getElementById('detallecompra-precioProducto').value;
    let precioventa = document.getElementById('detallecompra-PrecioVenta').value;
    let impuestocompra = document.getElementById('detallecompra-porcentajeImpuesto').value;
    let compraid = document.getElementById('detallecompra-id').value;
    let id = ""; //document.getElementById('detallecompra-id').value;
    let data = {'idDetalleCompra': id, 'compra' : {'idCompra': compraid }, 'producto' : {'idProducto': productoid}, 'cantidadProducto': cantidadproducto, 
                'precioProducto': precioproducto,'precioVenta': precioventa, 'porcentajeImpuesto': impuestocompra  }
    console.log(data); 
    let request = sendRequest('detallecompras/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        //document.getElementById("registra_producto").Modal.hide();
        //var modal = document.getElementById('registra_producto');
       //modal.hide;
        //leerDetalles(compraid);

        window.location = 'compras.html';
    } 
    request.onerror = function(){
        alert('Error al guardar los cambios')
    }
}

let sumaTotal = 0;
function leerDetalles(idCompra){
    
    let request = sendRequest('detallecompras/list/' , 'GET', '');
    //table.innerHTML = "";
    document.getElementById("detallecompra-id").value = idCompra;
    let valorIdCompra = document.getElementById('detallecompra-id');
    console.log("qqq",valorIdCompra);
    let table = document.getElementById('detallecompras-table')
    request.onload = function(){
        let data = request.response;
        if(data == null) {
            console.log("No data detallecompras")
        }
        else 
        {

            data.forEach((element, index) => {
                if (element.compra.idCompra == idCompra){
                    document.getElementById("btnGrabaFactura").style.visibility="visible";
                    var valorimp = (element.cantidadProducto * element.precioProducto) * element.porcentajeImpuesto/100
                    var valorxprod = (element.cantidadProducto * element.precioProducto) + valorimp;
                    sumaTotal = sumaTotal + valorxprod;
                    table.innerHTML += ` 
                        <tr >
                            <td>${element.idDetalleCompra}</td>
                            <td>${element.compra.idCompra}</td>
                            <td>${element.producto.descripcionProducto}</td>
                            <td>${element.cantidadProducto}</td>
                            <td>${element.precioProducto}</td>
                            <td>${element.precioVenta}</td>
                            <td>${element.porcentajeImpuesto}</td>
                            <td>${valorimp}</td>
                            <td>${valorxprod}</td>
                            <td>
                                <button type="button" class="btn  btn-link" onclick='deleteDetalleCompra(${element.idDetalleCompra})'>Borrar</button>
                            </td>
                        </tr>
                        `
                    }
              });
       }
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
            `;
    }
}

function deleteDetalleCompra(idDetalleCompra){
    let request = sendRequest('detallecompras/'+idDetalleCompra,'DELETE','')
    request.onload = function(){
        window.location = 'compras.html';
    }
}


