function loadData(){
    let request = sendRequest('productos/list', 'GET', '');
    let table = document.getElementById('productos-table');
    table.innerHTML = "";
    request.onload = function(){
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr >
                    <td>${element.idProducto}</td>
                    <td>${element.categoria.nombreCategoria}</td>
                    <td>${element.descripcionProducto}</td>
                    <td>${element.cantidadProducto}</td>
                    <td>${element.valorUnitarioProducto}</td>
                    <td>${element.porcentajeImpuesto}</td>
                    <td>${element.existenciaProducto}</td>
                    <td>${element.estadoProducto}</td>
                    <td>
                        <button type="button" class="btn  btn-link" 
                        onclick='window.location = "formproductos.html?id=${element.idProducto}"+"&idt=${element.categoria.idCategoria}"'>
                        Editar                            
                        </button>
                        <button type="button" class="btn btn-danger" 
                        onclick='deleteProducto(${element.idProducto})'>
                        Borrar                        
                        </button>
                    </td>
                </tr>
                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
            `;
    }
}

function saveProducto(){
    let categoria= document.getElementById('tipo').value; //producto-categoria
    let descripcion = document.getElementById('producto-descripcion').value;
    let cantidad = document.getElementById('producto-cantidad').value;
    let valorunitario= document.getElementById('producto-valorunitario').value;
    let porcentaje = document.getElementById('producto-porcentaje').value;
    let existencia = document.getElementById('producto-existencia').value;
    let estado= document.getElementById('producto-estado').value;
    let id = document.getElementById('producto-id').value;
    let data = {'idProducto': id,'categoria' : {'idCategoria': categoria }, 'descripcionProducto': descripcion, 'cantidadProducto': cantidad,'valorUnitarioProducto': valorunitario, 'porcentajeImpuesto': porcentaje,'existenciaProducto':  existencia, 'estadoProducto': estado }
    console.log(data);
    let request = sendRequest('productos/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'productos.html';
    } 
    request.onerror = function(){
        alert('Error al guardar los cambios')
    }
}

function deleteProducto(idProducto){
    let request = sendRequest('productos/'+idProducto,'DELETE','')
    request.onload = function(){
        window.location = 'productos.html';
    }
}

function loadProducto(idProducto,idTipo){
     console.log(idProducto, idTipo);
    let request = sendRequest('productos/list/'+idProducto,'GET','')
    let categoria= document.getElementById('tipo');    //producto-categoria
    let descripcion = document.getElementById('producto-descripcion');
    let cantidad = document.getElementById('producto-cantidad');
    let valorunitario = document.getElementById('producto-valorunitario');
    let porcentaje = document.getElementById('producto-porcentaje');
    let existencia = document.getElementById('producto-existencia');
    let estado= document.getElementById('producto-estado')
    let id= document.getElementById('producto-id')
    
   request.onload = function(){
        let data = request.response
        console.log(data)
        id.value = data.idProducto
        categoria.value = data.categoria
        descripcion.value = data.descripcionProducto       
        cantidad.value = data.cantidadProducto
        valorunitario.value = data.valorUnitarioProducto
        porcentaje.value = data.porcentajeImpuesto
        existencia.value = data.existenciaProducto       
        estado.value = data.estadoProducto
        loadCategorias(idTipo)
    }
    request.onerror = function(){
        alert("Error al recuperar los datos");
    }

    
}


