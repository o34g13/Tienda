CREATE DATABASE  IF NOT EXISTS `tienda` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `tienda`;
-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: tienda
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categoria` (
  `idCategoria` int NOT NULL AUTO_INCREMENT,
  `nombreCategoria` varchar(100) NOT NULL,
  `descripcionCategoria` varchar(200) NOT NULL,
  `estadoCategoria` tinyint(1) NOT NULL,
  PRIMARY KEY (`idCategoria`),
  UNIQUE KEY `idCategoria_UNIQUE` (`idCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'LACTEOS','Productos derivados de la leche',1),(2,'CEREALES','Productos fundamentales para la dieta diaria',1),(3,'CONDIMENTOS Y ESPECIES','Productos que son necesarios para dar color y sabor a los alimentos',1),(4,'ENLATADOS','Productos preservados para prolongar su duración',1),(5,'GRANOS','Productos secos de acompañamiento',1),(6,'LICORES','Productos derivados de alcohol',1),(7,'ASEO PERSONAL','Productos para la limpieza corporal',1),(15,'LIMPIEZA','Productos para la limpieza del hogar',1);
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compra`
--

DROP TABLE IF EXISTS `compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `compra` (
  `idCompra` int NOT NULL AUTO_INCREMENT,
  `idProveedor` int NOT NULL,
  `numeroFactura` varchar(30) NOT NULL,
  `fechaCompra` date DEFAULT NULL,
  `valorImpuesto` decimal(12,2) DEFAULT NULL,
  `valorCompra` decimal(12,2) DEFAULT NULL,
  `idUsuario` int NOT NULL,
  PRIMARY KEY (`idCompra`),
  UNIQUE KEY `idCompra_UNIQUE` (`idCompra`),
  KEY `fk_Compra_Persona_idx` (`idProveedor`) /*!80000 INVISIBLE */,
  KEY `fk_Compra_Usuario_idx` (`idUsuario`),
  CONSTRAINT `fk_Compra_Persona` FOREIGN KEY (`idProveedor`) REFERENCES `persona` (`idPersona`),
  CONSTRAINT `fk_Compra_Usuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compra`
--

LOCK TABLES `compra` WRITE;
/*!40000 ALTER TABLE `compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallecompra`
--

DROP TABLE IF EXISTS `detallecompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detallecompra` (
  `idDetalleCompra` int NOT NULL AUTO_INCREMENT,
  `idCompra` int NOT NULL,
  `idProducto` int NOT NULL,
  `cantidadProducto` decimal(8,2) NOT NULL,
  `precioProducto` decimal(12,2) NOT NULL,
  `precioVenta` decimal(12,2) NOT NULL,
  `porcentajeImpuesto` decimal(12,2) NOT NULL,
  PRIMARY KEY (`idDetalleCompra`),
  UNIQUE KEY `idDetalleCompra_UNIQUE` (`idDetalleCompra`),
  KEY `fk_DetalleCompra_Compra_idx` (`idCompra`),
  KEY `fk_DetalleCompra_Producto` (`idProducto`),
  CONSTRAINT `fk_DetalleCompra_Compra` FOREIGN KEY (`idCompra`) REFERENCES `compra` (`idCompra`),
  CONSTRAINT `fk_DetalleCompra_Producto` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallecompra`
--

LOCK TABLES `detallecompra` WRITE;
/*!40000 ALTER TABLE `detallecompra` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallecompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalleventa`
--

DROP TABLE IF EXISTS `detalleventa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detalleventa` (
  `idDetalleVenta` int NOT NULL AUTO_INCREMENT,
  `idVenta` int NOT NULL,
  `idProducto` int NOT NULL,
  `cantidadProducto` decimal(8,2) NOT NULL,
  `precioProducto` decimal(12,2) NOT NULL,
  `PrecioVenta` decimal(12,2) NOT NULL,
  `porcentajeImpuesto` decimal(12,2) NOT NULL,
  PRIMARY KEY (`idDetalleVenta`),
  UNIQUE KEY `idDetalleVenta_UNIQUE` (`idDetalleVenta`),
  KEY `fk_DetalleVenta_Venta_idx` (`idVenta`),
  KEY `fk_DetalleVenta_Producto_idx` (`idProducto`),
  CONSTRAINT `fk_DetalleVenta_Producto` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`),
  CONSTRAINT `fk_DetalleVenta_Venta` FOREIGN KEY (`idVenta`) REFERENCES `venta` (`idVenta`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalleventa`
--

LOCK TABLES `detalleventa` WRITE;
/*!40000 ALTER TABLE `detalleventa` DISABLE KEYS */;
INSERT INTO `detalleventa` VALUES (8,6,11,100.00,1222.00,1222.00,13.00),(9,6,1,22.00,12.00,12.00,1.00);
/*!40000 ALTER TABLE `detalleventa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona` (
  `idPersona` int NOT NULL AUTO_INCREMENT,
  `tipoPersona` int NOT NULL,
  `apellidoPersona` varchar(40) NOT NULL,
  `nombrePersona` varchar(60) NOT NULL,
  `tipoDocumentoPersona` varchar(3) NOT NULL,
  `numeroDocumentoPersona` varchar(30) NOT NULL,
  `direccionPersona` varchar(50) NOT NULL,
  `telefonoPersona` varchar(15) NOT NULL,
  `emailPersona` varchar(30) NOT NULL,
  PRIMARY KEY (`idPersona`),
  UNIQUE KEY `idPersona_UNIQUE` (`idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,1,'Proveedor 1','Proveedor Uno','CC','1111','Direccion 1','9999','hdrodriguezr@gmail.com'),(4,1,'Proveedor 2','Proveedor Dos','CE','2222','Direccion 2','9999','proveedor2@hotmail.com'),(5,2,'Cliente 1','Cliente Uno','NIT','222','Direccion 3','3333','dddd@cambiodos'),(6,1,'Proveedor 3','Proveedor Tres','CC','444','Direccion 4','1111','proveedor3@hotmail.com'),(7,2,'Cliente 2','Cliente 2','CC','22222','cra','444','dddd@cambiodos');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `producto` (
  `idProducto` int NOT NULL AUTO_INCREMENT,
  `idCategoria` int NOT NULL,
  `descripcionProducto` varchar(100) NOT NULL,
  `cantidadProducto` decimal(8,2) NOT NULL,
  `valorunitarioProducto` decimal(12,2) NOT NULL,
  `porcentajeImpuesto` decimal(12,2) NOT NULL,
  `existenciaProducto` decimal(12,2) NOT NULL,
  `estadoProducto` tinyint(1) NOT NULL,
  PRIMARY KEY (`idProducto`),
  UNIQUE KEY `idProducto_UNIQUE` (`idProducto`),
  KEY `fk_Producto_Categoria_idx` (`idCategoria`),
  CONSTRAINT `fk_Producto_Categoria` FOREIGN KEY (`idCategoria`) REFERENCES `categoria` (`idCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,1,'Leche',1000.00,3000.00,19.00,100.00,1),(6,6,'Cerveza',2000.00,4000.00,19.00,50.00,1),(8,5,'Lenteja',122.00,7000.00,16.00,60.00,1),(9,1,'Queso',1000.00,3000.00,19.00,100.00,1),(10,2,'Arroz',20000.00,2500.00,19.00,20000.00,1),(11,5,'Lenteja',122.00,7000.00,19.00,8888.00,1),(12,5,'Maiz',1221.00,8000.00,10.00,7000.00,1),(15,2,'Harina de Trigo',2000.00,2500.00,19.00,5000.00,1),(16,3,'Sal',100.00,1500.00,8.00,1500.00,1),(17,7,'Desodorante',300.00,4000.00,19.00,4000.00,1),(18,4,'Arveja Con Zanahoria',2000.00,6000.00,19.00,2000.00,1);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `idRol` int NOT NULL AUTO_INCREMENT,
  `descripcionRol` varchar(45) NOT NULL,
  PRIMARY KEY (`idRol`),
  UNIQUE KEY `idRol_UNIQUE` (`idRol`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'Administrador');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `idUsuario` int NOT NULL AUTO_INCREMENT,
  `tipoDocumento` int NOT NULL,
  `numeroDocumento` varchar(20) NOT NULL,
  `apellidoUsuario` varchar(40) NOT NULL,
  `nombreUsuario` varchar(40) NOT NULL,
  `claveUsuario` varchar(30) NOT NULL,
  `estadoUsuario` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Activo=1, Inactivo = 0',
  `idRol` int NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `idUsuario_UNIQUE` (`idUsuario`),
  UNIQUE KEY `numeroDocumento_UNIQUE` (`numeroDocumento`),
  KEY `fk_Usuario_Rol_idx` (`idRol`),
  CONSTRAINT `fk_Usuario_Rol` FOREIGN KEY (`idRol`) REFERENCES `rol` (`idRol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Tabla Usuarios ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (2,1,'2222','Administrador','Administrador','12345.',1,1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `venta` (
  `idVenta` int NOT NULL AUTO_INCREMENT,
  `idCliente` int NOT NULL,
  `numeroFactura` varchar(45) NOT NULL,
  `fechaVenta` date DEFAULT NULL,
  `valorImpuesto` decimal(12,2) DEFAULT NULL,
  `valorDescuento` decimal(12,2) DEFAULT NULL,
  `ValorVenta` decimal(12,2) DEFAULT NULL,
  `idUsuario` int NOT NULL,
  PRIMARY KEY (`idVenta`),
  UNIQUE KEY `idVenta_UNIQUE` (`idVenta`),
  KEY `fk_Venta_Persona_idx` (`idCliente`),
  CONSTRAINT `fk_Venta_Persona` FOREIGN KEY (`idCliente`) REFERENCES `persona` (`idPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta`
--

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;
INSERT INTO `venta` VALUES (6,5,'33333',NULL,0.00,0.00,0.00,2);
/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-20 22:39:14
