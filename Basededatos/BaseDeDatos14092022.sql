-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Tienda
-- -----------------------------------------------------
-- Base de Datso Tienda

-- -----------------------------------------------------
-- Schema Tienda
--
-- Base de Datso Tienda
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Tienda` ;
-- -----------------------------------------------------
-- Schema tienda
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tienda
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tienda` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `Tienda` ;

-- -----------------------------------------------------
-- Table `Tienda`.`Rol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tienda`.`Rol` (
  `idRol` INT NOT NULL AUTO_INCREMENT,
  `descripcionRol` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idRol`),
  UNIQUE INDEX `idRol_UNIQUE` (`idRol` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Tienda`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tienda`.`Usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `TipoDocumento` INT NOT NULL,
  `numeroDocumento` VARCHAR(20) NOT NULL,
  `apellidoUsuario` VARCHAR(40) NOT NULL,
  `nombreUsuario` VARCHAR(40) NOT NULL,
  `claveUsuario` VARCHAR(30) NOT NULL,
  `estadoUsuario` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Activo=1, Inactivo = 0',
  `idRol` INT NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE INDEX `idUsuario_UNIQUE` (`idUsuario` ASC) VISIBLE,
  UNIQUE INDEX `numeroDocumento_UNIQUE` (`numeroDocumento` ASC) VISIBLE,
  INDEX `fk_Usuario_Rol_idx` (`idRol` ASC) VISIBLE,
  CONSTRAINT `fk_Usuario_Rol`
    FOREIGN KEY (`idRol`)
    REFERENCES `Tienda`.`Rol` (`idRol`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tabla Usuarios ';


-- -----------------------------------------------------
-- Table `Tienda`.`Categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tienda`.`Categoria` (
  `idCategoria` INT NOT NULL AUTO_INCREMENT,
  `nombreCategoria` VARCHAR(100) NOT NULL,
  `descripcionCategoria` VARCHAR(200) NOT NULL,
  `estadoCategoria` TINYINT(1) NOT NULL,
  PRIMARY KEY (`idCategoria`),
  UNIQUE INDEX `idCategoria_UNIQUE` (`idCategoria` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Tienda`.`Producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tienda`.`Producto` (
  `idProducto` INT NOT NULL AUTO_INCREMENT,
  `idCategoria` INT NOT NULL,
  `descripcionProducto` VARCHAR(100) NOT NULL,
  `cantidadProducto` DECIMAL(8,2) NOT NULL,
  `valorUnitarioProducto` DECIMAL(12,2) NOT NULL,
  `porcentajeImpuesto` DECIMAL(12,2) NOT NULL,
  `existenciaProducto` DECIMAL(12,2) NOT NULL,
  `estadoProducto` TINYINT(1) NOT NULL,
  PRIMARY KEY (`idProducto`),
  UNIQUE INDEX `idProducto_UNIQUE` (`idProducto` ASC) VISIBLE,
  INDEX `fk_Producto_Categoria_idx` (`idCategoria` ASC) VISIBLE,
  CONSTRAINT `fk_Producto_Categoria`
    FOREIGN KEY (`idCategoria`)
    REFERENCES `Tienda`.`Categoria` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Tienda`.`Persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tienda`.`Persona` (
  `idPersona` INT NOT NULL AUTO_INCREMENT,
  `tipoPersona` INT NOT NULL,
  `apellidoPersona` VARCHAR(40) NOT NULL,
  `nombrePersona` VARCHAR(60) NOT NULL,
  `tipoDocumentoPersona` VARCHAR(3) NOT NULL,
  `numeroDocumentoPersona` VARCHAR(30) NOT NULL,
  `direccionPersona` VARCHAR(50) NOT NULL,
  `telefonoPersona` VARCHAR(15) NOT NULL,
  `emailPersona` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`idPersona`),
  UNIQUE INDEX `idPersona_UNIQUE` (`idPersona` ASC) VISIBLE,
  UNIQUE INDEX `tipoPersona_UNIQUE` (`tipoPersona` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Tienda`.`Venta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tienda`.`Venta` (
  `idVenta` INT NOT NULL AUTO_INCREMENT,
  `idCliente` INT NOT NULL,
  `numeroFactura` VARCHAR(45) NOT NULL,
  `fechaVenta` DATE NOT NULL,
  `valorImpuesto` DECIMAL(12,2) NOT NULL,
  `valorDescuento` DECIMAL(12,2) NOT NULL,
  `ValorVenta` DECIMAL(12,2) NOT NULL,
  `idUsuario` INT NOT NULL,
  PRIMARY KEY (`idVenta`),
  INDEX `fk_Venta_Persona_idx` (`idCliente` ASC) VISIBLE,
  UNIQUE INDEX `idVenta_UNIQUE` (`idVenta` ASC) VISIBLE,
  CONSTRAINT `fk_Venta_Persona`
    FOREIGN KEY (`idCliente`)
    REFERENCES `Tienda`.`Persona` (`idPersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Tienda`.`DetalleVenta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tienda`.`DetalleVenta` (
  `idDetalleVenta` INT NOT NULL AUTO_INCREMENT,
  `idVenta` INT NOT NULL,
  `idProducto` INT NOT NULL,
  `cantidadProducto` DECIMAL(8,2) NOT NULL,
  `precioProducto` DECIMAL(12,2) NOT NULL,
  `PrecioVenta` DECIMAL(12,2) NOT NULL,
  `porcentajeImpuesto` DECIMAL(12,2) NOT NULL,
  PRIMARY KEY (`idDetalleVenta`),
  UNIQUE INDEX `idDetalleVenta_UNIQUE` (`idDetalleVenta` ASC) VISIBLE,
  INDEX `fk_DetalleVenta_Venta_idx` (`idVenta` ASC) VISIBLE,
  INDEX `fk_DetalleVenta_Producto_idx` (`idProducto` ASC) VISIBLE,
  CONSTRAINT `fk_DetalleVenta_Venta`
    FOREIGN KEY (`idVenta`)
    REFERENCES `Tienda`.`Venta` (`idVenta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DetalleVenta_Producto`
    FOREIGN KEY (`idProducto`)
    REFERENCES `Tienda`.`Producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Tienda`.`Compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tienda`.`Compra` (
  `idCompra` INT NOT NULL AUTO_INCREMENT,
  `idProveedor` INT NOT NULL,
  `numeroFactura` VARCHAR(30) NOT NULL,
  `fechaCompra` DATE NOT NULL,
  `valorImpuesto` DECIMAL(12,2) NOT NULL,
  `valorCompra` DECIMAL(12,2) NOT NULL,
  `idUsuario` INT NOT NULL,
  PRIMARY KEY (`idCompra`),
  UNIQUE INDEX `idCompra_UNIQUE` (`idCompra` ASC) VISIBLE,
  INDEX `fk_Compra_Persona_idx` (`idProveedor` ASC) INVISIBLE,
  INDEX `fk_Compra_Usuario_idx` (`idUsuario` ASC) VISIBLE,
  CONSTRAINT `fk_Compra_Persona`
    FOREIGN KEY (`idProveedor`)
    REFERENCES `Tienda`.`Persona` (`idPersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Compra_Usuario`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `Tienda`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Tienda`.`DetalleCompra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Tienda`.`DetalleCompra` (
  `idDetalleCompra` INT NOT NULL AUTO_INCREMENT,
  `idCompra` INT NOT NULL,
  `idProducto` INT NOT NULL,
  `cantidadProducto` DECIMAL(8,2) NOT NULL,
  `precioProducto` DECIMAL(12,2) NOT NULL,
  `precioVenta` DECIMAL(12,2) NOT NULL,
  `porcentajeImpuesto` DECIMAL(12,2) NOT NULL,
  PRIMARY KEY (`idDetalleCompra`),
  UNIQUE INDEX `idDetalleCompra_UNIQUE` (`idDetalleCompra` ASC) VISIBLE,
  INDEX `fk_DetalleCompra_Compra_idx` (`idCompra` ASC) VISIBLE,
  CONSTRAINT `fk_DetalleCompra_Compra`
    FOREIGN KEY (`idCompra`)
    REFERENCES `Tienda`.`Compra` (`idCompra`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_DetalleCompra_Producto`
    FOREIGN KEY (`idProducto`)
    REFERENCES `Tienda`.`Producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `tienda` ;

-- -----------------------------------------------------
-- Table `tienda`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tienda`.`categoria` (
  `idCategoria` INT NOT NULL AUTO_INCREMENT,
  `nombreCategoria` VARCHAR(100) NOT NULL,
  `descripcionCategoria` VARCHAR(200) NOT NULL,
  `estadoCategoria` TINYINT(1) NOT NULL,
  PRIMARY KEY (`idCategoria`),
  UNIQUE INDEX `idCategoria_UNIQUE` (`idCategoria` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tienda`.`persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tienda`.`persona` (
  `idPersona` INT NOT NULL AUTO_INCREMENT,
  `tipoPersona` INT NOT NULL,
  `apellidoPersona` VARCHAR(40) NOT NULL,
  `nombrePersona` VARCHAR(60) NOT NULL,
  `tipoDocumentoPersona` VARCHAR(3) NOT NULL,
  `numeroDocumentoPersona` VARCHAR(30) NOT NULL,
  `direccionPersona` VARCHAR(50) NOT NULL,
  `telefonoPersona` VARCHAR(15) NOT NULL,
  `emailPersona` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`idPersona`),
  UNIQUE INDEX `idPersona_UNIQUE` (`idPersona` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tienda`.`rol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tienda`.`rol` (
  `idRol` INT NOT NULL AUTO_INCREMENT,
  `descripcionRol` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idRol`),
  UNIQUE INDEX `idRol_UNIQUE` (`idRol` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tienda`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tienda`.`usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `tipoDocumento` INT NOT NULL,
  `numeroDocumento` VARCHAR(20) NOT NULL,
  `apellidoUsuario` VARCHAR(40) NOT NULL,
  `nombreUsuario` VARCHAR(40) NOT NULL,
  `claveUsuario` VARCHAR(30) NOT NULL,
  `estadoUsuario` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Activo=1, Inactivo = 0',
  `idRol` INT NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE INDEX `idUsuario_UNIQUE` (`idUsuario` ASC) VISIBLE,
  UNIQUE INDEX `numeroDocumento_UNIQUE` (`numeroDocumento` ASC) VISIBLE,
  INDEX `fk_Usuario_Rol_idx` (`idRol` ASC) VISIBLE,
  CONSTRAINT `fk_Usuario_Rol`
    FOREIGN KEY (`idRol`)
    REFERENCES `tienda`.`rol` (`idRol`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
COMMENT = 'Tabla Usuarios ';


-- -----------------------------------------------------
-- Table `tienda`.`compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tienda`.`compra` (
  `idCompra` INT NOT NULL AUTO_INCREMENT,
  `idProveedor` INT NOT NULL,
  `numeroFactura` VARCHAR(30) NOT NULL,
  `fechaCompra` DATE NOT NULL,
  `valorImpuesto` DECIMAL(12,2) NOT NULL,
  `valorCompra` DECIMAL(12,2) NOT NULL,
  `idUsuario` INT NOT NULL,
  PRIMARY KEY (`idCompra`),
  UNIQUE INDEX `idCompra_UNIQUE` (`idCompra` ASC) VISIBLE,
  INDEX `fk_Compra_Persona_idx` (`idProveedor` ASC) INVISIBLE,
  INDEX `fk_Compra_Usuario_idx` (`idUsuario` ASC) VISIBLE,
  CONSTRAINT `fk_Compra_Persona`
    FOREIGN KEY (`idProveedor`)
    REFERENCES `tienda`.`persona` (`idPersona`),
  CONSTRAINT `fk_Compra_Usuario`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `tienda`.`usuario` (`idUsuario`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tienda`.`producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tienda`.`producto` (
  `idProducto` INT NOT NULL AUTO_INCREMENT,
  `idCategoria` INT NOT NULL,
  `descripcionProducto` VARCHAR(100) NOT NULL,
  `cantidadProducto` DECIMAL(8,2) NOT NULL,
  `valorUnitarioProducto` DECIMAL(12,2) NOT NULL,
  `porcentajeImpuesto` DECIMAL(12,2) NOT NULL,
  `existenciaProducto` DECIMAL(12,2) NOT NULL,
  `estadoProducto` TINYINT(1) NOT NULL,
  PRIMARY KEY (`idProducto`),
  UNIQUE INDEX `idProducto_UNIQUE` (`idProducto` ASC) VISIBLE,
  INDEX `fk_Producto_Categoria_idx` (`idCategoria` ASC) VISIBLE,
  CONSTRAINT `fk_Producto_Categoria`
    FOREIGN KEY (`idCategoria`)
    REFERENCES `tienda`.`categoria` (`idCategoria`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tienda`.`detallecompra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tienda`.`detallecompra` (
  `idDetalleCompra` INT NOT NULL AUTO_INCREMENT,
  `idCompra` INT NOT NULL,
  `idProducto` INT NOT NULL,
  `cantidadProducto` DECIMAL(8,2) NOT NULL,
  `precioProducto` DECIMAL(12,2) NOT NULL,
  `precioVenta` DECIMAL(12,2) NOT NULL,
  `porcentajeImpuesto` DECIMAL(12,2) NOT NULL,
  PRIMARY KEY (`idDetalleCompra`),
  UNIQUE INDEX `idDetalleCompra_UNIQUE` (`idDetalleCompra` ASC) VISIBLE,
  INDEX `fk_DetalleCompra_Compra_idx` (`idCompra` ASC) VISIBLE,
  INDEX `fk_DetalleCompra_Producto` (`idProducto` ASC) VISIBLE,
  CONSTRAINT `fk_DetalleCompra_Compra`
    FOREIGN KEY (`idCompra`)
    REFERENCES `tienda`.`compra` (`idCompra`),
  CONSTRAINT `fk_DetalleCompra_Producto`
    FOREIGN KEY (`idProducto`)
    REFERENCES `tienda`.`producto` (`idProducto`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tienda`.`venta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tienda`.`venta` (
  `idVenta` INT NOT NULL AUTO_INCREMENT,
  `idCliente` INT NOT NULL,
  `numeroFactura` VARCHAR(45) NOT NULL,
  `fechaVenta` DATE NOT NULL,
  `valorImpuesto` DECIMAL(12,2) NOT NULL,
  `valorDescuento` DECIMAL(12,2) NOT NULL,
  `ValorVenta` DECIMAL(12,2) NOT NULL,
  `idUsuario` INT NOT NULL,
  PRIMARY KEY (`idVenta`),
  UNIQUE INDEX `idVenta_UNIQUE` (`idVenta` ASC) VISIBLE,
  INDEX `fk_Venta_Persona_idx` (`idCliente` ASC) VISIBLE,
  CONSTRAINT `fk_Venta_Persona`
    FOREIGN KEY (`idCliente`)
    REFERENCES `tienda`.`persona` (`idPersona`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `tienda`.`detalleventa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tienda`.`detalleventa` (
  `idDetalleVenta` INT NOT NULL AUTO_INCREMENT,
  `idVenta` INT NOT NULL,
  `idProducto` INT NOT NULL,
  `cantidadProducto` DECIMAL(8,2) NOT NULL,
  `precioProducto` DECIMAL(12,2) NOT NULL,
  `PrecioVenta` DECIMAL(12,2) NOT NULL,
  `porcentajeImpuesto` DECIMAL(12,2) NOT NULL,
  PRIMARY KEY (`idDetalleVenta`),
  UNIQUE INDEX `idDetalleVenta_UNIQUE` (`idDetalleVenta` ASC) VISIBLE,
  INDEX `fk_DetalleVenta_Venta_idx` (`idVenta` ASC) VISIBLE,
  INDEX `fk_DetalleVenta_Producto_idx` (`idProducto` ASC) VISIBLE,
  CONSTRAINT `fk_DetalleVenta_Producto`
    FOREIGN KEY (`idProducto`)
    REFERENCES `tienda`.`producto` (`idProducto`),
  CONSTRAINT `fk_DetalleVenta_Venta`
    FOREIGN KEY (`idVenta`)
    REFERENCES `tienda`.`venta` (`idVenta`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
